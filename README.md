# a8FlowForm
  ```A8FlowForm``` manages all the usertasks by a8TaskList WebApp.
## Installation
 ```
 $ git clone https://gitlab.com/sathishautonom8/a8flowform.git
 $ cd a8flowform && npm install && npm start
 ```
 ## Documentation
- Prerequisites
- Form 
    - Form Creation
    - Field(Input Elements) Creation
    - How to bind task variabls to Fields(Input Elements)
    - Form Validation
    - Section Validation
    - How to trigger submit ```Pending```
    - How to trigger save ```Pending```
- Tab Creation
- Common Methods & Props Available At Form/Tab/Fields Level
- Currently Supporting Form Elements/Components for Field.

# Prerequisites
  > There are two ```environment variables``` available for a8flowform.
  > Env Variables are ```INSIDE``` ,```OUTSIDE```.
  > If you access a8FlowForm via IFRAM you will get env variable as ```INSIDE```.
  > If you access a8FlowForm without IFRAME you will get env variable as ```OUTSIDE``` .
  > You can access env variable in ```App.js``` using ```this.state.env```.
  > At Form level you can access via ```this.props.env```.
 
  #### Prerequisties for env = "INSIDE"
---
   Whenever you create ```flow(usertask)``` in ```CAMUNDA MODELER``` you need to specify 
   ```Form Key``` in specific format at ```userTask-> Forms -> Form Key field ``` like below.
   * FORMAT : ```a8apps:<APPNAME>:<VERSIONNAME>:<FORMNAME>```
     lets assume you plan to create ```applicant``` form first time for esaf, The FormKey should be
     ```a8apps:esaf:v1:applicant```

  #### Prerequisties for env = "OUTSIDE"
---
 If you plan to test your form before going to tasklist follow below steps:
  
   Custom Form Test -> go to App.js file and change the ```state.formMeta.formKey``` like below
   ```
     state = { formMeta: { formKey: "YOURFORMNAME" } }
   ```
   
 # Form Creation 
  Let assume you plan to create ```applicant``` form based on your ```Form Key``` configuration from ```CMM```
  ##### Sample Key : 
  Form key Pattern : ```a8apps:<APPNAME>:<VERSIONNAME>:<FORMNAME>```
  Sample Form Key :  ```a8apps:esaf:v1:applicant```
  
  #### Create the folder and file structure at ```src/forms/FORMNAME``` exactly like below.

``` 
src
├── forms
│   ├── applicant // create folder name same as FORMNAME from CMM
│   │   ├── Applicant.js//this is RootComponent first letter should be capital
         //contain all tab related configs
│   │   ├── applicantTabInfo.js // pattern for tabName FORMNAMETabInfo.js
         //contain all form related validation
│   │   ├── applicantValidator.js // pattern  for validatornFORMNAMEValidator.js
│   │   ├── index.js
│   │   ├── customTabOne.js //pattern for customtab tab<BusinessName>.js
│   │   └── customTabTwo.js 
```

# Field(Input Elements) Creation.
```
import { TextBox } from "../../helpers";
import {Field} from "redux-form";
  <Field
        label={"label"}
        name="FIELDNAME EXACTLY AS TASKVARIABLE NAME"
        component={TextBox} //FORM ELEMENTS YOU WANT TO RENDER
        placeholder="Enter Applicant Name"
        type="text" //ELEMENT TYPE
        hasFeedback // STATUS HIGHLIGHTED AT VALIDATION
        className="form-control-coustom" // CLASS NAMES YOU WANT TO PASS
        //EXTRA PROPS BASED ON COMPONENT YOU PASSED
     />
```

# How to bind task variabls to Fields(Input Elements).
   #### For ENVIRONMENT ```INSIDE``` 
   ---
   * Go To FormComponent.js and edit below code.
  ```
  const mapStateToProps = (state, props) => {
  return {
    /**
     * extraceInitialValues function helper extract
     * -the task variables and give the formate what initialValues understant
     * Note : initialValues is redux-form helper to set initialvalue for form fields
     * initialValues only accept {formKey : values, formKey : values } object format
     */
    initialValues: {
     // props.taskInfo.info.taskVariables come from tasklist app based on user task selected
      ...extractInitialValues(props.taskInfo.info.taskVariables),
      //add custom fields initial values
      customFieldName: "customFieldValue"
    }
  };
};
export default connect(mapStateToProps)(Applicant);
  ```
  
### For ENVIRONMENT ```OUTSIDE``` 
   ---
   * Go To src/app.js and edit below code.
   ```
     state = {
           taskInfo: { info: 
           { taskVariables: {
              CUSTOMFIELD : CUSTOMVALUE
              EXTRAFIELD : EXTRAVALUE   
           }}},
       };
   ```
   
 # How to add Form Validation

  ###  Step One : Create Form Validator
  Create Form Validator file if you did not, In the name of Form Key FORMNAME from ```CMM``` like below 
  path : ```src/form/FORMNAME/FORMNAMEValidator.js```
  ```
    import Validate from "validate.js";
    /**
     * This function execute each time the form fields change
     * PARAM (values) contain all the fields and values
     * you can GET the particular field by values.fieldName
     */
    export default (values) => {
      // errors object responsible for handling all errors info and return to redux-form
      const errors = {};
      //sample field checking
      if (Validate.isEmpty(values.ApplicationID)) {
        errors.ApplicationID = "ApplicationId is Required";
      }
      return errors;
    };
  ```
   ### Step Two : Attach Validator to listen the form change 

  * Go to your FormComponent(ex Applicant.js) and include validator 
    ```   
    import Validator from "./applicantValidator"
    ```
  * Find redux-form wrapper in you FormComponent and update like below
    ```
    //combine redux-form and FormComponent
    Applicant = reduxForm({
    validate: Validator,
    //leave extra fields if available
    })(Applicant);
    ```

  # Section Validation
 
  Section Validation help you build particular Form Section is ```PENDING``` OR ```COMPLETED``` inside 
  each Tab Header.
   ### Step One : Create Watcher Config:
 * We need to tell formSection what are the fileds we want to check, To change the status to ```pending``` or ```complete```
 * Go to your TabComponent and update the state like below 
 ```
   class TabOne extends Component {
      state = {
        /**
         * sectionValidator responsible for handling the particular formSection is valid or not
         * sectionValidator schema : { keyToCheckSection : [arrays of what are the fields we need to watch]}
         */
        sectionValidator: {
          basicInfo: ["ApplicationID", "BranchID", "extraField"],
          sectionTwo: ["BorrowerAddress"]
        }
      }
}
 ```
 
 ### Step Two : Pass Watcher Config to FormSection Component 
* Include FormHeadSection Widget from helper at tabComponent.js
 ```
import { FormHeadSection } from "../../helpers";
```
  * Pass the props like below to FormHeadSection component
  ```
     <FormHeadSection
              //text you want to display at tab right header
              sectionLabel="Basic Information"
              //text should match exactly this.state.sectionValidator watcher object key
              sectionKey="basicInfo"
              //formSyncError send sync errors based on current form change
              formSyncError={this.props.formSyncError}
              //pass sectionValidator watcher config
              sectionValidator={this.state.sectionValidator}
              //use this props to set firstTab always open
              initialTab={true}
            />
  ```

 # Tab Creation

  ### Step One : Create Tab Component with below structure
  * Tab FileName Format : tab<BusinessName>.js ex: tabGetUser
  * Class Name inside tabGetUser should be like this : class TabGetUser
  * Code Structure for creating tab
 
      ```
    import React, { Component } from "react";
    import { TextBox, FormHeadSection } from "../../helpers";
    import { Field, getFormSyncErrors, getFormValues } from "redux-form";
    import { connect } from "react-redux";
    //class name should be the format like Tab<BusinessName>
    class TabGetUser extends Component {
      state = {
        /**
         * sectionValidator responsible for handling the particular formSection is valid or not
         * sectionValidator schema : { keyToCheckSection : [arrays of what are the fields we need to watch]}
         */
        sectionValidator: {
          basicInfo: ["ApplicationID", "BranchID", "extraField"],
          sectionTwo: ["BorrowerAddress"]
        }
      };

      render() {
        return (
          <div className="tab-content">
            <div
              role="tabpanel"
              className="tab-pane active"
              id="card-item-details-1-lead"
            >
              //let's assume each form-section block as each card
              <div className="form-section">
                <FormHeadSection
                  sectionLabel="Basic Information"
                  sectionKey="basicInfo"
                  formSyncError={this.props.formSyncError}
                  sectionValidator={this.state.sectionValidator}
                  //use this props to set firstTab always open
                  initialTab={true}
                />
                <div className="form-section-content" style={{ display: "block" }}>
                  <div className="flex-row">
                    <div className="form-group col-xs-6 col-md-4">
                      <Field
                        label={"Applicant Id"}
                        name="ApplicationID"
                        component={TextBox}
                        placeholder="Enter Applicant Name"
                        type="text"
                        hasFeedback
                        className="form-control-coustom"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }
    
    const mapStateToProps = (state, props) => {
      return {
        //get current form values
        formValues: getFormValues("applicant")(state),
        //get current form sync errors
        formSyncError: getFormSyncErrors("applicant")(state)
      };
    };
    export default connect(mapStateToProps)(TabOne);
    ```
  ### Step Two : Update TabInfo file 
   * Update Newely creted tab to tab config file at src/forms/ ```FORMNAMETabInfo.js```
    Below snippets is base structure of tabInfo.js
   ```
    {
    // how many tabs we want to visible at primary level
    primaryTabVisibility: 5,
    // set which tab want to active inside tasklist
    activeTab: "tabOne",
    //add newely created TAB name using tabList config
    tabList: [
        {
            name: "NEWELY_CREATED_TAB_FILE_NAME",
            label: "LABEL",
        }
    ]
}
   ```
  
 ### Step Three : Update FormComponent
 * Update form component to render tab whenever tasklist tabs click 
 * Import your new tab component in FormComponent
 
     ```
     import TabUserCreation from "./tabUserCreation";
     ```
* add conditional expression in FormComponent render method like below
    ```
    render() {
        return (
          <React.Fragment>
            <Form>
              {this.state.activeTab === "tabUserCreation" && <TabUserCreation />}
            </Form>
          </React.Fragment>
        );
  }
    ```
    
## Common Methods & Props Available At Form/Tab/Fields Level.
---

All redux-form props and action creators supported by a8FlowForm.
[For more info about form helper methods Refer here.[REDUX-FORM REF]](https://redux-form.com/8.2.0/docs/api/).

| Method/Props |type | Scope  | Description |
| ------ | ------ | ------| -----|
| formMeta | prop | FormComponent | formMeta pass by app.js to FormComponent. It will contain info about formKey what you provide by ```CMM```|
| env |prop| FormComponent | env props pass by app.js to FormComponent. It will contain info about current environmant ex : ```INSIDE``` OR ```OUTSIDE``` |
|taskInfo | prop | FormComponent | taskInfo contain all info about userTask.
| extractInitialValues | method | FormComponent | This method live in helper folder, this method helps to extract initial values form taskInfo.taskVariables and convert the form what redux-form (react)  understand.
|Field, getFormSyncErrors, getFormValues, | method | TabComponent | read redux-form ref for more info.
|onChange|method|Field|you can use onChange method, Whenever need to perform custom logic based on field action.

# Currently Supporting Form Elements/Components for Field:

| Component List   | Status |
| ------ |  ------|
| TextBox | Done |
| TextArea | Done |
| Select |  ```pending``` |
| DatePicker |  ```pending``` |
| Switch |  ```pending```|
| Upload |  ```pending```|
| Radio |  ```pending```|
| Signature |  ```pending``` |





