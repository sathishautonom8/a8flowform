import React, { Component } from "react";
import { connect } from "react-redux";
import Validate from "validate.js";
import classname from "classnames";
import * as Forms from "./forms";

//import base styles
import "antd/dist/antd.css";
import "./assets/css/bootstrap.css";
import "./assets/css/icomoon.css";
import "./assets/css/style.css";
import "./App.css";

class App extends Component {
  state = {
    a8FlowPointer: null,
    taskInfo: { info: { taskVariables: {} } },
    initialTrigger: true,
    formMeta: {
      formKey: "applicant"
    },
    error: {
      status: false,
      info: null
    },
    env: "OUTSIDE"
  };
  componentDidMount() {
    //add the eventlistener
    window.addEventListener("message", this.a8FlowListener);
  }
  componentWillUnmount() {
    //end the eventlistener
    window.removeEventListener("message", this.a8FlowListener);
  }

  //this function responsible for listening the post messages
  a8FlowListener = e => {
    try {
      this.setState({
        a8FlowPointer: e,
        error: {
          status: false,
          info: null
        }
      });
       
      console.log('FORMFLOWIFRAMLISTENER');
      console.log(e);

      let { data, origin } = e;
      if (origin === "http://localhost:3000") {
        console.log('IFRME LISTENER TRIGGERED');
        //set environment variable
        this.setState({
          env: "INSIDE"
        });
        //if taskVariables found setTask variables to state
        if (!Validate.isEmpty(data.taskInfo)) {
          //get formkey to send appropriate tabs info
          let {
            info: { formKey }
          } = data.taskInfo;
          if (Validate.isEmpty(formKey)) {
            throw "A8Flow:FormKey is empty please provide formKey while you design the flow @ CAMUNDA MODELER";
          }

          //destructuring formkey
          let [brand, client, version, form] = formKey.split(":");
          if (Forms[`${form}TabInfo`]) {
            this.state.a8FlowPointer.source.postMessage(
              {
                tabInfo: Forms[`${form}TabInfo`]
              },
              "http://localhost:3000"
            );
            this.setState({
              formMeta: {
                brand,
                client,
                version,
                formKey: form
              },
              taskInfo: data.taskInfo
            });
          } else {
            this.setState({
              error: {
                status: true,
                info:
                  "A8Flow:Error Tab Configs not found for your formKey please check!"
              }
            });
            throw "A8Flow:Error Tab Configs not found for your formKey please check!";
          }
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    let Form = Forms[this.state.formMeta.formKey];

    if (this.state.error.status) {
      return <h1>{this.state.error.info}</h1>;
    }

    if (!Form)
      return (
        <h1>Your Form is not found. please check the formKey is correct!</h1>
      );

    return (
      <React.Fragment>
        <div
          className={classname({
            outSideWrapperStyle: this.state.env === "OUTSIDE"
          })}
        >
          <Form
            formMeta={this.state.formMeta}
            env={this.state.env}
            taskInfo={this.state.taskInfo}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
