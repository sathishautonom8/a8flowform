import Validate from "validate.js";
export const extractInitialValues = values => {
  /**
   * return empty object is values is empty or invalid
   */
  if (Validate.isEmpty(values)) return {};

  /**
   * check value is object or not
   * if not object return empty object
   */
  if (!Validate.isObject) return {};

  // filtered values holder
  let extractedValues = {};

  //extrace value based on redux-form initialValues {formName : values}
  for (var key in values) {
    extractedValues[key] = values[key].value;
  }
  //return extracted values
  return extractedValues;
};

export const SectionValidator = ({
  sectionName,
  formSyncError = {},
  sectionValidator = []
}) => {
  try {
    if (sectionValidator[sectionName]) {
      let section = sectionValidator[sectionName];
      //if sectionName found, Process the formSyncError and check if any errors found for this sectionNames fields
      for (let key in formSyncError) {
        if (section.indexOf(key) != -1) {
          return false;
        }
      }
      return true;
    } else {
      //if your sectionName not inside sectionValidator return false
      throw `Your sectionName does not found inside this.state.sectionValidator. Please include your ${sectionName} config @ this.state.sectionValidator`;
    }
  } catch (error) {
    console.error(error);
    return false;
  }
};
