# How to use Form Widgets in a8flowform
---
 # Currently Supporting Form Elements/Components for Field:
| Component List   | Status |
| ------ |  ------|
| TextBox | Done |
| TextArea | Done |
| Select |  Done |
| DatePicker | Done |
| Switch |  ```pending``` |
| Upload |  ```pending``` |
| Radio |  ```pending``` |
| Signature |  ```pending``` |


 ### Input Text/number/password widget example.
 ---
   import from helper like below 
  ``` import {TextBox} from "../../helpers"; ```
     
    <Field
         label={customLabelName}
         name={fieldName}
         component={TextBox}
         placeholder="placeholder"
         type="text"
         hasFeedback // optional
         onChange={()=>{}} // optional
         // below className is required
         className="form-control-coustom" 
    />
  ### TextArea widget example.
---
  import from helper like below 
  ``` import {TextAreaHelper} from "../../helpers";```

    <Field
        label={customLabelName}
        name={fieldName}
        component={TextAreaHelper}
        placeholder="placeholder"
        rows={4} //optional
        hasFeedback // optional
        onChange={()=>{}} // optional
        // below className is required
        className="form-control-coustom" 
        //Height autosize feature
        autosize={boolean} // optional
    /> 

   ### Select widget example.
 ---
   import from helper like below 
  ``` import {SelectHelper} from "../../helpers";```

     <Field
      label="{customLabelName}"
      name="{fieldName}"
      component={SelectHelper}
      placeholder="PLACEHOLDER"
      //below classname is required 
      className="a8Select">
        <Option value="1">Option One</Option>
        <Option value="2">Option Two</Option>
        <Option value="3">Option Three</Option>
    </Field>

    ### DatePicker widget example.
 ---
   import from helper like below 
  ``` import {DatePickerHelper} from "../../helpers";```

     <Field 
         label="customlabelname"
         name={fieldName}
         component={DatePickerHelper}
         showTime //optional
         placeholder="Select Time"
    />