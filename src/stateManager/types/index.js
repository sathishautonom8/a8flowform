import authTypes from "./auth.type";
import uiTypes from "./ui.type";
import taskTypes from "./task.type";
export default {
    ...authTypes,
    ...uiTypes,
    ...taskTypes
}