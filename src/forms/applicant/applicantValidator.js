import Validate from "validate.js";
/**
 * This function execute each time the form fields change
 * values contain all the fields and values
 * you can check the fields by values.fieldName
 */
export default values => {
  // errors object responsible for handling all errors info and return to redux-form
  const errors = {};

  if (Validate.isEmpty(values.ApplicationID)) {
    errors.ApplicationID = "ApplicationId is Required";
  }
  if (!values.BranchID) {
    errors.BranchID = "Branch Id is Required";
  }
  if (!values.BorrowerAddress) {
    errors.BorrowerAddress = "BorrowerAddress is Required";
  }
  if(!values.extraField){
      errors.extraField = "Extra field is required"
  }
  return errors;
};
