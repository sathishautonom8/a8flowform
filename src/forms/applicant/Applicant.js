import React, { Component } from "react";
import config from "./applicantTabInfo";
import Validator from "./applicantValidator";
import { Form, Button } from "antd";
import { reduxForm, submit } from "redux-form";
import { connect } from "react-redux";
import { extractInitialValues } from "../../helpers";
import Style from "./Applicant.module.css";
//Tab Components
import TabOne from "./tabOne";
import TabTwo from "./tabTwo";

const rsubmit = (values)=>{
  console.log('FROM REMOTE SUBMIT');
  console.log(values);
}

class Applicant extends Component {
  state = {
    activeTab: config.activeTab,
    taskInfo: null
  };

  componentDidMount = () => {
    console.log("FROM THIS PROPS");
    console.log(this.props);
    window.addEventListener("message", this.formPMListener);
  };

  // DO NOT MODIFY THIS CODE
  formPMListener = e => {
    try {
      let { data, origin } = e;
      if (origin === "http://localhost:3000") {
        if (data.activeTab) {
          this.setState({
            activeTab: data.activeTab
          });
        }

        if(data.formAction){
          if(data.formAction == "submit"){
            //perform submit action
            this.props.submit("applicant")
          }
        }
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  componentWillUnmount = () => {
    window.removeEventListener("message", this.formPMListener);
  };

  handleForm = values => {
    console.log("from handle form");
  };

  render() {
    /**
     * This below extraction are form handling available hooks
     * use it when ever necessery ...
     * hooks are available by this.props.HOOKNAME
     */
    const { handleSubmit, pristine, reset, submitting, invalid } = this.props;
    return (
      <React.Fragment>
        <Form
          layout={"vertical"}
          className="login-form"
          onSubmit={handleSubmit(this.handleForm)}
        >
          {this.state.activeTab === "tabOne" && <TabOne />}
          {this.state.activeTab === "tabTwo" && <TabTwo />}
          {this.props.env === "OUTSIDE" && (
            <div style={{ width: "20%", marginTop: "16px" }}>
              <Button
                type="primary"
                htmlType="submit"
                className="button button-primary"
                style={{ marginTop: "9px" }}
                // disabled={invalid || pristine || submitting}
                // loading={this.state.btnLoading}
              >
                Submit
              </Button>
            </div>
          )}
        </Form>
      </React.Fragment>
    );
  }
}

//combine redux-form and component
Applicant = reduxForm({
  //pass your form key
  form: "applicant",
  validate: Validator,
  enableReinitialize: true,
  destroyOnUnmount: false,
  onSubmit : rsubmit
})(Applicant);

const mapStateToProps = (state, props) => {
  return {
    /**
     * extraceInitialValues function helper extract
     * -the task variables and give the formate what initialValues understant
     * Note : initialValues is redux-form helper to set initialvalue for form fields
     * initialValues only accept {formKey : values, formKey : values } object format
     */
    initialValues: {
      ...extractInitialValues(props.taskInfo.info.taskVariables),
      selectWidget : null,
      //add custom fields initial values
      // extraField: "value"
    }
  };
};

export default connect(
  mapStateToProps,
  {submit}
)(Applicant);


