export default {
    primaryTabVisibility: 5,
    activeTab: "loan",
    tabList: [
        {
            name: "loan",
            label: "loan",
        },
        {
            name: "sampleLoanOne",
            label: "SampleOne"
        }, {
            name: "sampleLoanTwo",
            label: "sampleTwo"
        },
        {
            name: "sampleThree",
            label: "sampleThree"
        },
        {
            name: "sampleFour",
            label: "sampleFour"
        },
        {
            name: "sampleFive",
            label: "sampleFive"
        },
        {
            name: "sampleSix",
            label: "sampleSix"
        },
        {
            name: "sampleSeven",
            label: "sampleSeven"
        },
        {
            name: "sampleEight",
            label: "sampleEight"
        },
        {
            name: "sampleNine",
            label: "sampleNine"
        },
        {
            name: "sampleTen",
            label: "sampleTen"
        }
    ]
}